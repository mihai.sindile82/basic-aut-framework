package Utils;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import org.apache.commons.io.FileUtils;
import org.apache.commons.net.ntp.TimeStamp;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.*;
import java.util.Properties;
import java.util.Set;

public class OtherUtils {

    public static void logInfoStatus(ExtentTest test, String message) {
        if (test != null)
            test.log(Status.INFO, message);
        System.out.println(message);
    }

    public static void printCookie(Cookie c){
        System.out.println("Cookie " + c.getName() + ": " + c.getValue());
        System.out.println("Expiry date, if set: " + c.getExpiry());
    }

    public static int getCoookieNumber(WebDriver driver){
        Set<Cookie> cookies = driver.manage().getCookies();
        System.out.println("The number of cookies found are: " + cookies.size());
        return cookies.size();
    }

    public static void printCookies(WebDriver driver){
        Set<Cookie> cookies = driver.manage().getCookies();
        System.out.println("The number of cookies found are: " + cookies.size());
        for (Cookie c : cookies){
            System.out.println("Cookie " + c.getName() + ": " + c.getValue());
            System.out.println("Expiry date, if set: " + c.getExpiry());
        }
    }

    public static void takeScreenShot(WebDriver driver){
        TimeStamp timeStamp = new TimeStamp(System.currentTimeMillis());
        File screenshotFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        File finalFile = new File(SeleniumUtils.screenShotPath + "\\screenshot-" + timeStamp.getTime() + ".png");
        try {
            FileUtils.copyFile(screenshotFile, finalFile);
        } catch (IOException e) {
            System.out.println("File cannot be saved");
        }
    }

    public static String sanitizeNullDbString(String dbResult){
        if(dbResult == null){
            return "";
        }
        return dbResult;
    }

    public static Properties readPropertiesFile(String path) throws IOException {

        InputStream input = new FileInputStream(path);
        Properties prop = new Properties();
        prop.load(input);
        return prop;
    }

}
