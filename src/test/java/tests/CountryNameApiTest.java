package tests;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.core.IsEqual.equalTo;
import java.util.Arrays;
import java.util.List;

public class CountryNameApiTest extends BaseControllerTest{

    @Test
    public void getCountryNameInfo(){
        Response response =
                given().log().uri().when().
                        get("{apiType}/{apiVersion}/name/{countryName}", apiType, apiVersion, "Romania").
                        then().
                        statusCode(200).
                        contentType(JSON).
                        body("name[0]", equalTo("Romania")).
                        extract().
                        response();

        response.body().prettyPrint();
    }

    @Test
    public void verifyCountryCapital(){
        Response response = given().log().uri().when().
                get("{apiType}/{apiVersion}/name/{countryName}", apiType, apiVersion, "Romania").
                then().
                statusCode(200).
                body("capital[0]", equalTo("Bucharest")).
                extract().
                response();
        response.body().prettyPrint();
    }

    @Test
    public void verifyBorders(){
        List<String> expectedBorders = Arrays.asList("BGR","HUN","MDA","SRB","UKR");
        List<String> borders = given().log().uri().when().
                get("{apiType}/{apiVersion}/name/{countryName}", apiType, apiVersion, "Romania").
                then().
                statusCode(200).
                contentType(JSON).
                extract().jsonPath().getList("borders[0]", String.class);
        System.out.println("Actual borders:" + borders);

        Assert.assertTrue(Arrays.deepEquals(borders.toArray(), expectedBorders.toArray()));
    }

    @Test
    public void verifyAltSpellings(){
        String altSpel = "România";
        List<String> altSpellings = given().log().uri().when().
                get("{apiType}/{apiVersion}/name/{countryName}", apiType, apiVersion, "Romania").
                then().
                statusCode(200).
                contentType(JSON).
                extract().jsonPath().getList("altSpellings[0]", String.class);

        Assert.assertTrue(altSpellings.contains(altSpel));
        System.out.println("List contains: " + altSpel);
        //alternative
        given().log().uri().when().
                get("{apiType}/{apiVersion}/name/{countryName}", apiType, apiVersion, "Romania").
                then().
                statusCode(200).
                contentType(JSON).
                body("altSpellings[0]", hasItem("România")).
                body("altSpellings[0]", hasItem("RO"));
    }

    @Test
    public void verifyMultipleCurrencies(){
                given().log().uri().when().
                get("{apiType}/{apiVersion}/name/{countryName}", apiType, apiVersion, "Antarctica").
                then().
                statusCode(200).
                contentType(JSON).
                body("currencies[0][0].symbol", equalTo("$")).
                body("currencies[0].symbol", hasItem("£"));
    }
}
