package tests;

import Utils.OtherUtils;
import io.restassured.RestAssured;
import org.testng.annotations.BeforeClass;

import java.io.IOException;
import java.util.Properties;

public class BaseControllerTest {
    String apiHostname;
    String apiType;
    String apiVersion;

    @BeforeClass
    public void serUpApi(){
        try {
            Properties prop = OtherUtils.readPropertiesFile("src\\test\\java\\framework.properties");
            apiHostname = prop.getProperty("apiHostname");
            System.out.println("Use the next hostname:" + apiHostname);
            apiType = prop.getProperty("apiType");
            System.out.println("Type:" + apiType);
            apiVersion = prop.getProperty("apiVersion");
            System.out.println("Version:" + apiVersion);

            // set-up rest-assured based hostname

            RestAssured.baseURI = apiHostname;
            RestAssured.useRelaxedHTTPSValidation();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }


    }
}
